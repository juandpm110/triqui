var turno="X";
var jugadas=0;

//pUNTO DE ENTRADA AL PROYECTO
function iniciarjuego(){
    
    //crear 9 botones

    let miboton;
    let formulario;
    let i;
    let salto;

    //crear un objeto que referencie al formulario
    formulario = document.getElementById("formtablero");

    for(i=1;i<10;i++){
    //crear un objeto de tipo input
    miboton = document.createElement("input");

    //agregar atributos al objeto
    miboton.setAttribute("type" , "button");
    miboton.setAttribute("id" , "boton" + i);
    miboton.setAttribute("class" , "boton");
    miboton.setAttribute("value" , " ");
    miboton.setAttribute("onclick" , "realizarjugada(this.id)");

    formulario.appendChild(miboton);

    if(i%3==0){
        salto = document.createElement("br");
        formulario.appendChild(salto);
    }

    }
    
}

function realizarjugada(elemento){
    
    let resultado, triqui;
    resultado = llenarcasilla(elemento);
    if(resultado == true){
        jugadas++;
        triqui = calculartriqui();
        if(triqui){
            alert("Hay triqui");
            borrartablero();
        }else{
            if(jugadas == 9){
                alert("Empate");
                borrartablero();
            }else{
                cambiarturno();
            }
        }
    }
}

function cambiarturno(){
    if (turno === "X") {
        turno = "O";
      } else {
        turno = "X";
      }
}

function llenarcasilla(elemento){
    let casilla = document.getElementById(elemento);
  if (casilla.value === "X" || casilla.value === "O") {
    alert("Casilla ocupada. Por favor seleccione otra.");
    return false;
  }
  casilla.value = turno;
  return true;
}

function calculartriqui(){
    let i, casilla1, casilla2, casilla3;

    for(i=0;i<9;i=i+3){
        casilla1 = document.getElementById("boton" + (i+1)).value;
        casilla2 = document.getElementById("boton" + (i+2)).value;
        casilla3 = document.getElementById("boton" + (i+3)).value;
        if(casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " "){
            return true;
        }
    }

    for(i=1;i<4;i++){
        casilla1 = document.getElementById("boton" + i).value;
        casilla2 = document.getElementById("boton" + (i+3)).value;
        casilla3 = document.getElementById("boton" + (i+6)).value;
        if(casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " "){
            return true;
        }
    }

    for(i=0;i<2;i++){
        let valor = [2, 4];
        casilla1 = document.getElementById("boton" + 5).value;
        casilla2 = document.getElementById("boton" + (5 + valor[i])).value;
        casilla3 = document.getElementById("boton" + (5 - valor[i])).value;
        if(casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " "){
            return true;
        }
    }

    return false;
}

function borrartablero(){
    jugadas = 0;
    turno = "X";

    for(i=1;i<10;i++){
        boton = document.getElementById("boton" + i);
        boton.value = " ";
    }
}